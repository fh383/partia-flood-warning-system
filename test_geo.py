'tests for functions in the geo module'

from floodsystem import geo
import numpy as np
from floodsystem.station import MonitoringStation

def test_stations_by_distance():
    # Create a station
    s_id = "test-s1-id"
    m_id = "test-m-id"
    label = "some station"
    coord = (45.7597, 4.8422)
    trange = (-2.3, 3.4445)
    river = "River X"
    town = "My Town"
    s1 = MonitoringStation(s_id, m_id, label, coord, trange, river, town)

    # Create a station
    s_id = "test-s2-id"
    m_id = "test-m-id"
    label = "some station"
    coord = (48.8567, 2.3508)
    trange = (-2.3, 3.4445)
    river = "River X"
    town = "My Town"
    s2 = MonitoringStation(s_id, m_id, label, coord, trange, river, town)

    london = (51.5074, 0.1278)

    stat_dist = geo.stations_by_distance([s1,s2], london)
    assert stat_dist[0][0] == s2
    assert stat_dist[1][0] == s1
    assert np.isclose(stat_dist[0][1], 334.5, atol= 1)
    assert np.isclose(stat_dist[1][1], 726.6, atol = 1)
    
    return


    # Create a station
    s_id = "test-s1-id"
    m_id = "test-m-id"
    label = "some station1"
    coord = (45.7597, 4.8422)
    trange = (-2.3, 3.4445)
    river = "River X"
    town = "My Town"
    s1 = MonitoringStation(s_id, m_id, label, coord, trange, river, town)

    # Create a station
    s_id = "test-s2-id"
    m_id = "test-m-id"
    label = "some station"
    coord = (48.8567, 2.3508)
    trange = (-2.3, 3.4445)
    river = "River X"
    town = "My Town"
    s2 = MonitoringStation(s_id, m_id, label, coord, trange, river, town)

    london = (51.5074, 0.1278)

    #use stations_within_radius to get list of close_stations
    close_stations = geo.stations_within_radius([s1,s2], london, 500)

    assert close_stations == [s2]
    
    return

def test_stations_within_radius():
    # Create a station
    s_id = "test-s1-id"
    m_id = "test-m-id"
    label = "some station1"
    coord = (45.7597, 4.8422)
    trange = (-2.3, 3.4445)
    river = "River X"
    town = "My Town"
    s1 = MonitoringStation(s_id, m_id, label, coord, trange, river, town)

    # Create a station
    s_id = "test-s2-id"
    m_id = "test-m-id"
    label = "some station"
    coord = (48.8567, 2.3508)
    trange = (-2.3, 3.4445)
    river = "River X"
    town = "My Town"
    s2 = MonitoringStation(s_id, m_id, label, coord, trange, river, town)

    london = (51.5074, 0.1278)

    #use stations_within_radius to get list of close_stations
    close_stations = geo.stations_within_radius([s1,s2], london, 500)

    assert close_stations == [s2]
    
    return

def test_rivers_with_station():
    # Create dummy stations
    dummy1 = MonitoringStation("dummy-id","dummy-m-id","dummy1", None,None, "River A", "Town A")
    dummy2 = MonitoringStation("dummy-id2","dummy-m-id2","dummy2",None,None,None,"Town B")
    dummy3 = MonitoringStation("dummy-id3","dummy-m-id3","dummy3", None,None, "River A", "Town C")
    dummy4 = MonitoringStation("dummy-id4","dummy-m-id4","dummy4", None,None, "River B", "Town D")
    stations = [dummy1,dummy2,dummy3,dummy4,dummy4]#Test with repeats and invalid 
    #Implement rivers_with station
    rivers = geo.rivers_with_station(stations)
    assert len(rivers)==2#Only 2 unique rivers
    assert rivers[0] == "River A" and rivers[1] == "River B"#Rivers should be in alphabetical order
    assert None not in rivers#Should not include invalid river
    
    return

def test_stations_by_river():
    # Create dummy stations
    dummy1 = MonitoringStation("dummy-id","dummy-m-id","dummy1", None,None, "River A", "Town A")
    dummy2 = MonitoringStation("dummy-id2","dummy-m-id2","dummy2",None,None,"River A","Town B")
    dummy3 = MonitoringStation("dummy-id3","dummy-m-id3","dummy3", None,None, "River A", "Town C")
    dummy4 = MonitoringStation("dummy-id4","dummy-m-id4","dummy4", None,None, "River B", "Town D")
    stations = [dummy1,dummy3,dummy2,dummy4,dummy4]#Add duplicate, switch order
    rivers_to_stations = geo.stations_by_river(stations)
    assert len(rivers_to_stations.items()) == 2#Should be 2 different rivers
    assert rivers_to_stations['River A'] == ['dummy1','dummy2','dummy3']#check alphabetical order
    assert rivers_to_stations['River B'] == ['dummy4']#check no duplicates
    
    return

def test_rivers_by_station_number():
    # Create dummy stations
    dummy1 = MonitoringStation("dummy-id","dummy-m-id","dummy1", None,None, "River A", "Town A")
    dummy2 = MonitoringStation("dummy-id2","dummy-m-id2","dummy2",None,None,"River A","Town B")
    dummy3 = MonitoringStation("dummy-id3","dummy-m-id3","dummy3", None,None, "River A", "Town C")
    dummy4 = MonitoringStation("dummy-id4","dummy-m-id4","dummy4", None,None, "River B", "Town D")
    dummy5 = MonitoringStation("dummy-id5","dummy-m-id5","dummy5", None,None, "River B", "Town E")
    dummy6 = MonitoringStation("dummy-id6","dummy-m-id6","dummy6", None,None, "River C", "Town F")
    dummy7 = MonitoringStation("dummy-id7","dummy-m-id7","dummy7", None,None, "River C", "Town G")
    dummy8 = MonitoringStation("dummy-id8","dummy-m-id8","dummy8", None,None, "River D", "Town H")
    stations = [dummy1,dummy2,dummy3,dummy4,dummy5,dummy6,dummy7,dummy8]#Extra stations for thorough testing
    rivers_number_stations = geo.rivers_by_station_number(stations,2)#top two numbers of stations per river
    
    assert len(rivers_number_stations)==3#Tie for second place, river C included as well
    assert "River D" not in [i[0] for i in rivers_number_stations]#check D not included
    assert rivers_number_stations[0][0]=='River A' and rivers_number_stations[2][0]=='River C'#check correct order
    
    return