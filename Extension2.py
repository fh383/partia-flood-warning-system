'''Demonstrations program for Extension task 1'''

from floodsystem.geo import map_stations_levels
from floodsystem.stationdata import build_station_list

def run():
    stations = build_station_list()
    map_stations_levels(stations)

if __name__ == "__main__":
    print("*** Extension2 : CUED Part IA Flood Warning System ***")
    run()
