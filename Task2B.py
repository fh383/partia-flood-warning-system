'''Demonstration program for task 2B'''

from floodsystem.station import MonitoringStation
from floodsystem.stationdata import build_station_list, update_water_levels
from floodsystem.flood import stations_level_over_threshold

def run():
    #fetch station data as stations
    stations = build_station_list()

    #update water level
    update_water_levels(stations)

    #finds stations which have a relavive tol over 0.8
    stations_over_threshold = stations_level_over_threshold(stations, 0.8)

    #prints station name, rel tol for each station in stations_over_threshold
    for station_and_level in stations_over_threshold:
        print(station_and_level[0].name, station_and_level[1])

if __name__ == "__main__":
    print("*** Task 2B: CUED Part IA Flood Warning System ***")
    run()