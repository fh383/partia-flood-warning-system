from floodsystem.analysis import polyfit
import numpy as np
import datetime
def test_polyfit():
    p_coeff = np.polyfit([1,2,3],[1,4,9],2)
    poly = np.poly1d(p_coeff)
    assert np.isclose(poly(4),16,atol=1e-05)
    try:
        polyfit([1],[2],2)
    except Exception as E:
        assert type(E) == TypeError
    try:
        polyfit([datetime.datetime.now()],[1,2],2)
    except Exception as E:
        assert type(E) == ValueError
