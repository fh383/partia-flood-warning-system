"""Provide a program file Task1F.py that builds a list of all stations 
with inconsistent typical range data. Print a list of station names, 
in alphabetical order, for stations with inconsistent data. """
from floodsystem.stationdata import build_station_list
from floodsystem.station import inconsistent_typical_range_stations
def run():
    #Generate station list
    stations = build_station_list()
    #Call function (that uses station method to check for invalid ranges) and store result in list 
    inconsistent = sorted(inconsistent_typical_range_stations(stations))
    #Print result
    print("The inconsistent stations are:")
    print(inconsistent)
if __name__ == "__main__":
    print("*** Task 1F: CUED Part IA Flood Warning System ***")
    run()