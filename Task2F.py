"""Task 2F
for each of the 5 stations at which the current relative water level is greatest and for a time period extending back 2 days,
 plots the level data and the best-fit polynomial of degree 4 against time. Show the typical range low/high on your plot.
"""
import datetime
from floodsystem.plot import plot_water_level_with_fit
from floodsystem.datafetcher import fetch_measure_levels
from floodsystem.stationdata import build_station_list,update_water_levels
from floodsystem.flood import stations_highest_rel_level
import matplotlib.pyplot as plt
def run():
    stations = build_station_list(False)
    ##Copied from Task2E
    update_water_levels(stations)
    top_stations = stations_highest_rel_level(stations, 5)#can change to any number 1-6, plot function will still work
    dt = 2 # Changed for demonstration program
    dates = []
    levels = []
    for station in top_stations:
        datess, levelss = fetch_measure_levels(station.measure_id, dt=datetime.timedelta(days=dt))#new temp. variables
        dates.append(datess)
        levels.append(levelss)
    for station in top_stations:#loop through top 5
        #print(station.relative_water_level())
        n= top_stations.index(station)#store current index
        plot_water_level_with_fit(station,dates[n],levels[n],p=4)#plot polynomial fit, order 4
        plt.axhline(station.typical_range[0],label = 'low',linestyle='-.',color='red')#display typical low and high
        plt.axhline(station.typical_range[1],label = 'high',linestyle='-.',color='red')#aside - data typically >> high (top 5 stations used)
        plt.show()
if __name__ == "__main__":
    print("*** Task 2F: CUED Part IA Flood Warning System ***")
    run()