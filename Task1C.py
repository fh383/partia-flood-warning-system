#Demonstration program for task 1C. Prints alphebetical list of stations within 10 km of cambridge city centre

from floodsystem.geo import stations_within_radius
from floodsystem.stationdata import build_station_list
from floodsystem.station import MonitoringStation

def run():
    centre = (52.2053, 0.1218)
    stations_10k = stations_within_radius(build_station_list(), centre, 10)
    stations = []
    for station in stations_10k:
        stations.append(station.name)
    stations.sort()
    print("The stations within a 10km radius of Cambridge are:")
    print(stations)
    return

if __name__ == "__main__":
    print("*** Task 1C: CUED Part IA Flood Warning System ***")
    run()