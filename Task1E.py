"""
Demonstration program
Provide a program file Task1E.py that prints the list of (river,number stations) tuples when N = 9. 
The list has more then 9 entries since a number of rivers have 14 stations."""
from floodsystem.geo import rivers_by_station_number
from floodsystem.stationdata import build_station_list

def run():
    #Generate list of stations
    stations = build_station_list()
    N=9
    #Call rivers_by_station to return the top N (inclusive at end) rivers
    rivers = rivers_by_station_number(stations,N)
    print("The list of (river, number stations) tuples when N=9:")
    for i in rivers:
        print(i)

if __name__ == "__main__":
    print("*** Task 1E: CUED Part IA Flood Warning System ***")
    run()