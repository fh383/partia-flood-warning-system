'''Unit test for the flood module'''

from floodsystem.station import MonitoringStation
from floodsystem.flood import stations_level_over_threshold, stations_highest_rel_level

def test_stations_level_over_threshold():
    
    #create some test stations. 4 stations needed to test correct order and correct rejections
    #create station 1
    s_id = "test-s-id1"
    m_id = "test-m-id"
    label = "some station"
    coord = (-2.0, 4.0)
    trange = (0.0, 10.0)
    river = "River X"
    town = "My Town"
    s1 = MonitoringStation(s_id, m_id, label, coord, trange, river, town)
    s1.latest_level = None

    #create station 2
    s_id = "test-s-id2"
    m_id = "test-m-id"
    label = "some station"
    coord = (-2.0, 4.0)
    trange = (0.0, 10.0)
    river = "River X"
    town = "My Town"
    s2 = MonitoringStation(s_id, m_id, label, coord, trange, river, town)
    s2.latest_level = 5.0

    #create station 3
    s_id = "test-s-id3"
    m_id = "test-m-id"
    label = "some station"
    coord = (-2.0, 4.0)
    trange = (0.0, 10.0)
    river = "River X"
    town = "My Town"
    s3 = MonitoringStation(s_id, m_id, label, coord, trange, river, town)
    s3.latest_level = 7.5

    #create station 4
    s_id = "test-s-id4"
    m_id = "test-m-id"
    label = "some station"
    coord = (-2.0, 4.0)
    trange = (0.0, 10.0)
    river = "River X"
    town = "My Town"
    s4 = MonitoringStation(s_id, m_id, label, coord, trange, river, town)
    s4.latest_level = 2.5

    stations = [s1, s2, s3, s4] #create list of stations
    #execute stations_level_over_threshhold to get list of tuples of staions over tolerance
    tol_stations = stations_level_over_threshold(stations=stations, tol=0.4) 

    #check fuction has retured correct values
    assert len(tol_stations) == 2
    assert tol_stations[0][0] == s3
    assert tol_stations[1][0] == s2

def test_stations_highest_rel_level():
    #create some test stations:station 1
    s_id = "test-s-id1"
    m_id = "test-m-id"
    label = "some station"
    coord = (-2.0, 4.0)
    trange = (0.0, 10.0)
    river = "River X"
    town = "My Town"
    s1 = MonitoringStation(s_id, m_id, label, coord, trange, river, town)
    s1.latest_level = 0.0
    #create station 2
    s_id = "test-s-id2"
    m_id = "test-m-id"
    label = "some station"
    coord = (-2.0, 4.0)
    trange = (0.0, 10.0)
    river = "River X"
    town = "My Town"
    s2 = MonitoringStation(s_id, m_id, label, coord, trange, river, town)
    s2.latest_level = 5.0

    #create station 3
    s_id = "test-s-id3"
    m_id = "test-m-id"
    label = "some station"
    coord = (-2.0, 4.0)
    trange = (0.0, 10.0)
    river = "River X"
    town = "My Town"
    s3 = MonitoringStation(s_id, m_id, label, coord, trange, river, town)
    s3.latest_level = 7.5

    #create station 4
    s_id = "test-s-id4"
    m_id = "test-m-id"
    label = "some station"
    coord = (-2.0, 4.0)
    trange = (0.0, 10.0)
    river = "River X"
    town = "My Town"
    s4 = MonitoringStation(s_id, m_id, label, coord, trange, river, town)
    s4.latest_level = 2.5

    stations = [s1,s2,s3,s4]
    assert stations_highest_rel_level(stations, 1) == [s3]
    assert stations_highest_rel_level(stations, 3) == [s3, s2, s4]
