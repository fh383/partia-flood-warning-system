'''Demonstrations program for Extension task 1'''

from floodsystem.geo import map_stations
from floodsystem.stationdata import build_station_list

def run():
    stations = build_station_list()
    map_stations(stations)

if __name__ == "__main__":
    print("*** Extension1 : CUED Part IA Flood Warning System ***")
    run()
