"""Provide a program file Task2E.py that plots the water levels
 over the past 10 days for the 5 stations at which the current relative water level is greatest. """
import datetime
from floodsystem.datafetcher import fetch_measure_levels
from floodsystem.stationdata import build_station_list,update_water_levels
from floodsystem.flood import stations_highest_rel_level
from floodsystem.plot import plot_water_levels
def run():
    stations = build_station_list()
    update_water_levels(stations)
    top_5_stations = stations_highest_rel_level(stations, 5)#can change to any number 1-6, plot function will still work
    dt = 10 # number of days data is plotted for (most recent N)
    dates = []#lists of dates and levels, to be filled for each station
    levels = []
    for station in top_5_stations:
        datess, levelss = fetch_measure_levels(station.measure_id, dt=datetime.timedelta(days=dt))#new temp. variables
        dates.append(datess)
        levels.append(levelss)
    plot_water_levels(top_5_stations, dates,levels)#modified plot, accomodates 1-6 stations + corresponding lists
if __name__ == "__main__":
    print("*** Task 2E: CUED Part IA Flood Warning System ***")
    run()
