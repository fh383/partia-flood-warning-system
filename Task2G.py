from floodsystem.stationdata import build_station_list,update_water_levels
import matplotlib.pyplot as plt
import numpy as np
def run():
    """
    Demonstration program - Using your implementation, 
    list the towns where you assess the risk of flooding
    to be greatest. Explain the criteria that you have 
    used in making your assessment, and rate the risk at
    ‘severe’, ‘high’, ‘moderate’ or ‘low’.
    ##Calculating median, LQ, UQ by trial and error (2sf accuracy)
    """
    stations = build_station_list()
    update_water_levels(stations)#attempt to update water levels from None, leave as None otherwise
    rel_levels = []#compiling all relative water levels
    for station in stations:
        if station.relative_water_level() != None:#checking valid river levels (not None)
            rel_levels.append(station.relative_water_level())
    low,moderate,high,severe= np.percentile(rel_levels,[50,65,75,90])#setting risk thresholds
    #arbitrary(?) division points, discuss
    #dividing into quartiles does not make sense - 75% of towns have relative water levels below 0.84, which seems low-moderate risk 
    #Look at desktop plot of distribution of relative water levels.
    at_risk_towns = []
    for station in stations:
        if station.relative_water_level() != None and station.relative_water_level()>severe:
            at_risk_towns.append(station.town)
    print("The towns in the top 10 percentile of relative water levels are:",list(set(at_risk_towns)),'\n')#remove duplicates
    print("For these towns, the relative water level is greater than",severe.round(3),"and we assess them to be at greatest risk of flooding.")
    """
    If looking at trend in last few days using polyfit function, must use low order polynomial, as high order will have large fluctuations
    at boundaries. Then, can say that if slope > -2 (arbitrary), classify as moderate instead of high, or high instead of severe. Hard to 
    make a quantitative cutoff point at which we can safely say that the town is safe in the near future.
    Ultimately, think that present values should be used to determine present risk, not forecast future values, which always have some 
    uncertainty associated with them (the weather being driven by random forces).
    model function to assess risk (not implemented,trivial); add as a method to MonitoringStation, with arguments (self,moderate,severe,high)
        def rate_risk(station):
        l = station.relative_water_level()
        if l!= None:
            if l < moderate:
                return "Low"
            elif l < high:
                return "Moderate"
            elif l < severe:
                return "High"
            return "Severe"
        return "Invalid level"""
    
    #----test plots ----
    #plt.hist(np.array(sorted(rel_levels)),100,range=(-10,10))
    #plt.axvline(1,color='red')
    #plt.plot(rel_levels)
    #plt.show()
if __name__ == "__main__":
    print("*** Task 2G: CUED Part IA Flood Warning System ***")
    run()