#Demonstration program for Task1B. Prints a list of tuples (station_name, town, distance) for the 10 closest and 10 furthest stations
#from cambridge city centre

from floodsystem.geo import stations_by_distance
from floodsystem.stationdata import build_station_list

def run():
    #coords of city centre
    city_coord = (52.2053, 0.1218)

    #fetch list of MoniteringStation objects
    stations = build_station_list()

    #ordered list of tuples (station, distance)
    station_dist = stations_by_distance(stations, city_coord)

    #create list of tuples of 10 closest and furthest stations
    closest_stations = []
    for i in range(10):
        station = station_dist[i][0]
        distance = station_dist[i][1]
        closest_stations.append( (station.name, station.town, distance) )

    furthest_stations = []
    for i in range(10):
        station = station_dist[-1*i][0]
        distance = station_dist[-1*i][1]
        furthest_stations.append( (station.name, station.town, distance) )

    print("The 10 closest stations are:", closest_stations)
    print("The 10 furthest stations are:", furthest_stations)
    return

if __name__ == "__main__":
    print("*** Task 1B: CUED Part IA Flood Warning System ***")
    run()