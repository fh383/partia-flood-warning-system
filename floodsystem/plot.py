import matplotlib.pyplot as plt
try:
    from .analysis import polyfit
except ModuleNotFoundError:
    from analysis import polyfit
def plot_water_levels(stations,dates,levels):
    """
    Arguments: list of MonitoringStation objects, list of list of dates
    list of list of levels. Lengths of all three range from 1 to 6.
    Plots water levels for selected stations and dates, in
    individual subplots, changing depending on the length of the lists.
    """
    if len(stations) != len(dates) or len(stations) != len(levels) or len(dates) != len(levels):
        raise SystemError("Input lists not of the same length")
    if len(stations) == 0 or len(stations) >6:
        raise ValueError("Too many/too few stations")
    if len(stations) == 1:
        plt.plot(dates[0],levels[0])
        plt.xlabel('date')
        plt.ylabel('water level (m)')
        plt.title(stations[0].name)
        plt.xticks(rotation=45)
        plt.tight_layout()
    elif len(stations) == 2:
        for station in stations:
            n= stations.index(station)
            plt.subplot(1,2,n+1)
            plt.plot(dates[n],levels[n])
            plt.xlabel('date')
            plt.ylabel('water level (m)')
            plt.title(station.name)
            plt.xticks(rotation=45)
            plt.tight_layout()
    elif len(stations) == 3 or len(stations) ==4:
        for station in stations:
            n= stations.index(station)
            plt.subplot(2,2,n+1)
            plt.plot(dates[n],levels[n])
            plt.xlabel('date')
            plt.ylabel('water level (m)')
            plt.title(station.name)
            plt.xticks(rotation=45)
            plt.tight_layout()
    elif len(stations) == 5 or len(stations) ==6:
        for station in stations:
            n= stations.index(station)
            plt.subplot(2,3,n+1)
            plt.plot(dates[n],levels[n])
            plt.xlabel('date')
            plt.ylabel('water level (m)')
            plt.title(station.name)
            plt.xticks(rotation=45)
            plt.tight_layout()
    plt.show()

def plot_water_level_with_fit(station,dates,levels,p):
    try:
        if len(dates) != len(levels):
            raise ValueError("Unequal list lengths")
        if type(station) == None:
            raise TypeError("Invalid station")
    except TypeError:
        raise TypeError
    polyfit(dates,levels,p)
    plt.title(station.name)