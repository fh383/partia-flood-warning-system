# Copyright (C) 2018 Garth N. Wells
#
# SPDX-License-Identifier: MIT
"""This module provides a model for a monitoring station, and tools
for manipulating/modifying station data

"""
#from .stationdata import update_water_levels


class MonitoringStation:
    """This class represents a river level monitoring station"""

    def __init__(self, station_id, measure_id, label, coord, typical_range,
                 river, town):

        self.station_id = station_id
        self.measure_id = measure_id

        # Handle case of erroneous data where data system returns
        # '[label, label]' rather than 'label'
        self.name = label
        if isinstance(label, list):
            self.name = label[0]

        self.coord = coord
        self.typical_range = typical_range
        self.river = river
        self.town = town
        # New attribute to store most recent water level
        self.latest_level = None
        # dateOpened,status
    def typical_range_consistent(self):
        """Checks the typical high/low range data for consistency. (i) no data (ii) high < low
        Returns True if the data is consistent and False if the data is inconsistent or unavailable."""
        if self.typical_range == None or self.typical_range[0]>self.typical_range[1]:#Fail conditions
            return False
        return True#True otherwise(data exists and high>low)

    def __repr__(self):
        d = "Station name:     {}\n".format(self.name)
        d += "   id:            {}\n".format(self.station_id)
        d += "   measure id:    {}\n".format(self.measure_id)
        d += "   coordinate:    {}\n".format(self.coord)
        d += "   town:          {}\n".format(self.town)
        d += "   river:         {}\n".format(self.river)
        d += "   typical range: {}".format(self.typical_range)
        return d

    def relative_water_level(self):
        '''returns the latest water level as a fraction of the typical range,
        i.e. a ratio of 1.0 corresponds to a level at the typical high and a 
        ratio of 0.0 corresponds to a level at the typical low. The waterlevel
        must have been updated using stationdata.update_water_levels() first'''
        #update_water_levels()
        if self.typical_range_consistent() != True or type(self.latest_level) != float: #check latest_level has correct type typical_range is consistant
            return None
        ranges = self.typical_range
        level = float(self.latest_level)
        return (level - ranges[0]) / (ranges[1] - ranges[0]) #calculate relative water level

def inconsistent_typical_range_stations(stations):
    """Implement in the submodule station a function that, given a list of station objects, 
    returns a list of stations that have inconsistent data. Uses attribute 
    MonitoringStation.typical_range_consistent"""
    #Uses list comprehension for conciseness - logic is evident
    return [station.name for station in stations if station.typical_range_consistent()==False]