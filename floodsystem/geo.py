# Copyright (C) 2018 Garth N. Wells
#
# SPDX-License-Identifier: MIT
"""This module contains a collection of functions related to
geographical data.

"""

from .utils import sorted_by_key  # noqa
import numpy as np
from haversine import haversine
from .station import MonitoringStation
import operator #for sorting dict by value
from .stationdata import update_water_levels
#imports for mapping
from bokeh.io import output_file, show
from bokeh.models import ColumnDataSource, GMapOptions
from bokeh.plotting import gmap

def stations_by_distance(stations, p):
    '''Takes arguments of a list of MonitoringStation objects, stations, 
    and lat-long coords, p. Returns an ordered list of tuples (station, 
    distance), where distance is the distance between the station and p.
    List is order by distance from min to max'''
    stat_dist = []
    try:
        if len(p) != 2 or len(stations[0].coord) != 2:
            raise ValueError("Input lat-long coords are wrong length")
        if type(p[0]) != float or type(p[1]) != float:
            raise ValueError("Input lat-long coords for p are wrong type")
    except ValueError:
        print("Input for stations_by_distance fuction is invalid")
    try:
        for station in stations:
            stat_dist.append( (station, haversine(station.coord, p)) )
        return sorted_by_key(stat_dist, 1, False)
    except TypeError:
        print("Input type for stations_by_distance is wrong, should be float for lat-long coords")
        return
        
def stations_within_radius(stations, centre, r):
    '''Takes arguments of list of MonitoringStation objects, stations,
     lat-long coords, centre and radius from centre, r. Returns list of stations 
     within radius r of centre'''
    #Check input data is right type
    try:
        assert type(centre) == tuple
        for i in centre:
            assert type(i) == float or int
        assert len(centre) == 2
        assert type(r) == float or int
        assert type(stations) == list
        assert type(stations[0]) == MonitoringStation
    except AssertionError:
        print("Wrong input type for stations, centre or r")
        return

    #generate ordered list of tuples (station, distance)
    stations_dist = stations_by_distance(stations, centre)

    #check which stations are within r of centre so which have distance <= r and add them to list close_stations
    close_stations = []
    i = 0
    while stations_dist[i][1] <= r and i <= len(stations_dist): #stations_dist is ordered low to high so only checks untill dist > r
        close_stations.append(stations_dist[i][0])
        i += 1
    return close_stations

def rivers_with_station(stations):
    """Given a list of MonitoringStation objects,stations, returns a list 
    with the names of the rivers that have monitoring stations. 
    Does not contain duplicates."""
    out = []#create output list
    for station in stations:
        #Check if there is a river associated with the station
        if station.river!= None:
            #If there is a river, add that river to the output list
            out.append(station.river)
    #return an alphabetically sorted list, minus duplicates
    return sorted(list(set(out)))
    
def stations_by_river(stations):
    """Takes an argument of a list of MonitoringStation objects, stations.
    Returns a Python dict that maps river names to a list of
     station objects on each river, alphabetically sorted."""
    result ={}#create output dictionary
    for station in stations:
        #if river is already in output
        if station.river in result:
            new = result[station.river]#duplicate existing entry under key:river name
            new.append(station.name)#add station to the new entry list
            result[station.river] = new#save new list
        else:
            #create new key if river not in keys already, with attached station name
            result[station.river]=[station.name]
    #Want the dict to be alphabetical, for easier understanding when printing result
    sort = sorted(result)#order the rivers dictionary alphabetically, by key value
    final ={}#and redefine a dict for the output
    for r in sort:
        final[r]=sorted(set(result[r]))#Alphabetically sorted, removed duplicates of stations
    return final

def rivers_by_station_number(stations,N):
    """Takes arguments of a list of MonitoringStation objects, stations, and integer N.
    Function that determines the N rivers with the greatest number of monitoring stations.
    It returns a list of (river name, number of stations) tuples, sorted by 
    the number of stations. In the case that there are more rivers with the 
    same number of stations as the N th entry, includes these rivers in the list."""
    #Use previously defined function to generate list of rivers and stations on each river
    rivers_to_stations = stations_by_river(stations)
    rivers_to_no = []#Convert list of stations to number of stations (length)
    for i in rivers_to_stations:
        rivers_to_no.append((i,len(rivers_to_stations[i])))
    #Sort the list by number of stations on river (tuple index 1), descending
    sorted_rivers = sorted(rivers_to_no,key=operator.itemgetter(1),reverse=True)
    answer = sorted_rivers[0:N]#first N values of sorted list
    for i in range(N,len(sorted_rivers)):
        #check the remaining rivers if they have the same no. of stations
        #as the nth cutoff, and add them to answer if they do.
        if sorted_rivers[i][1]==answer[-1][1]:
            answer.append(sorted_rivers[i])
    return answer

def map_stations(stations):
    '''Argument: list of MonitoringStation objects.
    Produces a map that shows all the stations'''
    #find lat & long of all stations
    lats = []
    longs = []
    for station in stations:
        co = station.coord
        lats.append(co[0])
        longs.append(co[1])

    output_file("gmap.html")

    map_options = GMapOptions(lat=53.131162, lng=-1.938465, map_type="roadmap", zoom=6)

    p = gmap("AIzaSyA2ilAMNcBwEk4QmbWMehqbECOUqh0AinY", map_options, title="Weather stations in the UK")

    source = ColumnDataSource(data=dict(lat=lats, long=longs))

    p.circle(x="long", y="lat", size=5, fill_color="black", fill_alpha=0.5, source=source)

    show(p)
def map_stations_levels(stations):
    '''Argument: list of MonitoringStation objects.
    Produces a map that shows all the stations'''
    #find lat & long of all stations
    output_file("gmap.html")
    map_options = GMapOptions(lat=53.131162, lng=-1.938465, map_type="roadmap", zoom=6)
    p = gmap("AIzaSyA2ilAMNcBwEk4QmbWMehqbECOUqh0AinY", map_options, title="Weather stations in the UK")
    update_water_levels(stations)
    lats_in_range = []
    longs_in_range = []
    lats_below_range = []
    longs_below_range = []
    lats_above_range = []
    longs_above_range = []
    lats_no_range = []
    longs_no_range = []
    for station in stations:
        co = station.coord
        if station.relative_water_level() == None:
            lats_no_range.append(co[0])
            longs_no_range.append(co[1])
        elif station.relative_water_level() < station.typical_range[0]:
            lats_below_range.append(co[0])
            longs_below_range.append(co[1])
        elif station.relative_water_level() > station.typical_range[1]:
            lats_above_range.append(co[0])
            longs_above_range.append(co[1])
        elif station.relative_water_level() >= station.typical_range[0] and station.relative_water_level() <= station.typical_range[1]:
            lats_in_range.append(co[0])
            longs_in_range.append(co[1])
    source_no_range = ColumnDataSource(data=dict(lat=lats_no_range, long=longs_no_range))
    source_in_range = ColumnDataSource(data=dict(lat=lats_in_range, long=longs_in_range))
    source_below_range = ColumnDataSource(data=dict(lat=lats_below_range, long=longs_below_range))
    source_above_range = ColumnDataSource(data=dict(lat=lats_above_range, long=longs_above_range))
    p.circle(x="long", y="lat", size=5, fill_color="white", fill_alpha=0.5, source=source_no_range)
    p.circle(x="long", y="lat", size=5, fill_color="green", fill_alpha=0.5, source=source_in_range)
    p.circle(x="long", y="lat", size=5, fill_color="blue", fill_alpha=0.5, source=source_below_range)
    p.circle(x="long", y="lat", size=5, fill_color="red", fill_alpha=0.5, source=source_above_range)
    show(p)