""" Submodule introduced for Task 2F.
implement a function that given the water level time history (dates, levels) 
for a station computes a least-squares fit of a polynomial of degree p to water level data.
 The function should return a tuple of (i) the polynomial object and (ii) any shift of the 
 time (date) axis"""
import matplotlib.pyplot as plt
import numpy as np
import matplotlib
import datetime
def polyfit(dates,levels,p):
    """
    Arguments: list of dates (datetime objects), list of (absolute) water levels,
    degree of fit polynomial
    Computes polynomial object and shift of datetime
    """
    try:
        assert isinstance(dates[0],datetime.datetime)
    except AssertionError:
        raise TypeError("Incorrect date type")
    if len(dates)!= len(levels):
        raise ValueError("Unequal list lengths")
    x = matplotlib.dates.date2num(dates[::-1])
    new_levels = levels[::-1]
    p_coeff = np.polyfit(x-x[0],new_levels,p)
    poly = np.poly1d(p_coeff)
    plt.plot(x,new_levels,'.')
    plt.xlabel('date, shifted '+str(x[0])+' days')
    plt.ylabel('water level (m)')
    x1 = np.linspace(x[0],x[-1],30)
    plt.plot(x1,poly(x1-x[0]))
    return (poly,x[0])