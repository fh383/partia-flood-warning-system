'''contains a collection of functions for assesing flood risk'''

from .station import MonitoringStation
from .utils import sorted_by_key
from floodsystem.stationdata import build_station_list, update_water_levels

def stations_level_over_threshold(stations, tol):
    '''takes stations: a list of MoniteringStation objects, and tol: a float as arguments. Returns a list of tuples of 
    station, relative_water_level for all stations with a relative_water_level > tol. Must update water level before 
    using this function'''

    out = [] #output list
    for station in stations: #iterates through all stations to check if their relative-water_level >= tol, then if true, adds them to out
        if station.typical_range_consistent() == True:
            rel = station.relative_water_level()
            if type(rel) == float:
                if rel > tol:
                    out.append( (station, rel) )
    return sorted_by_key(out, 1, reverse=True) #sorts list in relative desending order of relative_water_level and returns them

def stations_highest_rel_level(stations, N):
    '''Takes arguments stations: a list of MoniteringStation objects and N an integer. 
    Returns N stations with the highest water level in descending order as a list of
    MoniteringStation objects. Must update water levels before using this function'''

    #raise Value error if N is negiative of greater than number of stations
    if N > len(stations) or N < 0:
        raise ValueError("N is larger than number of MoniteringStation objects, or a negative number")

    #remove stations with invalid values
    rel = [( station, station.relative_water_level() ) for station in stations]
    rel_sorted = sorted_by_key(rel, 1, reverse=True) #sort rel into descending order of relative_water_level
    return [out[0] for out in rel_sorted[:N]] #return list of N Monitoring stations objects