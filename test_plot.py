from floodsystem.plot import plot_water_levels, plot_water_level_with_fit
from floodsystem.stationdata import update_water_levels, build_station_list
from floodsystem.flood import stations_highest_rel_level
from floodsystem.datafetcher import fetch_measure_levels
import datetime
def test_plot_water_levels():
    stations = build_station_list()
    update_water_levels(stations)
    top_5_stations = stations_highest_rel_level(stations,5)
    top_7_stations = stations_highest_rel_level(stations, 7)#can change to any number 1-6, plot function will still work
    dt = 10 # number of days data is plotted for (most recent N)
    dates = []#lists of dates and levels, to be filled for each station
    levels = []
    for station in top_7_stations:
        datess, levelss = fetch_measure_levels(station.measure_id, dt=datetime.timedelta(days=dt))#new temp. variables
        dates.append(datess)
        levels.append(levelss)
    try:
        plot_water_levels(top_7_stations, dates,levels)
    except Exception as E:#check that too many stations
        assert type(E) == ValueError
    try:
        plot_water_levels(top_5_stations,[1,2,3,4,5],[1,2,3,4])
    except Exception as E:#check that incorrect list lengths
        assert type(E) == SystemError

def test_plot_water_levels_with_fit():
    stations = build_station_list()
    update_water_levels(stations)
    top_5_stations = stations_highest_rel_level(stations,5)
    station = top_5_stations[0]
    dates,levels = fetch_measure_levels(station.measure_id,dt=datetime.timedelta(days=1))
    levelss = levels.copy().append(1)
    try:
        plot_water_level_with_fit(station,dates,levelss,p=3)
    except Exception as E:
        assert type(E) == TypeError
            