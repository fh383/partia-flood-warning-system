from floodsystem.geo import rivers_with_station,stations_by_river
from floodsystem.stationdata import build_station_list
def run():
    """Demonstration program Task1D:
    uses geo.rivers_with_station to print out how many rivers have at least one
    monitoring station, and prints the first 10 of them in alphabetical order
    uses geo.stations_by_river to print the names of the stations
    located on 'River Aire','River Cam', River Thames'"""
    #Generate list of stations
    stations = build_station_list()
    #Use geo.rivers_with_station to make 
    rivers = rivers_with_station(stations)
    #The number of rivers with at least one monitoring station is the length of the list
    print("The number of rivers with at least one monitoring station is: "+str(len(rivers)))
    #Print the first 10(the list is already in alphabetical order)
    print("The first 10 rivers are: ",rivers[0:10])

    #find stations by all rivers
    rivers_to_station = stations_by_river(stations)
    #if river is 'River Aire','River Cam', or 'River Thames', print stations on that river
    print("For River Aire, River Cam, and River Thames, the stations are:")
    for river in rivers_to_station:
        if river in ['River Aire','River Cam', 'River Thames']:
            print(river,rivers_to_station[river])
if __name__ == "__main__":
    print("*** Task 1D: CUED Part IA Flood Warning System ***")
    run()