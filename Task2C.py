from floodsystem.station import MonitoringStation
from floodsystem.stationdata import build_station_list, update_water_levels
from floodsystem.flood import stations_highest_rel_level

def run():
    #fetch station data as stations
    stations = build_station_list()

    #update water level
    update_water_levels(stations)

    #finds 10 stations with highest water level
    top_N_stations = stations_highest_rel_level(stations, 10)

    #prints station name, rel tol for each station in stations_over_threshold
    for station_and_level in top_N_stations:
        print(station_and_level.name, station_and_level.relative_water_level())

if __name__ == "__main__":
    print("*** Task 2C: CUED Part IA Flood Warning System ***")
    run()